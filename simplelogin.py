#!/usr/bin/env python

"""
BEERWARE LICENSE (Revision 43):

<machduck@yandex.ru> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
"""

from flask import abort
from flask import session, request

from functools import wraps
from types import MethodType

class Strategy( object ):
	def strategy( self, fn, strategy ):
		object.__setattr__( self, fn, MethodType( strategy, self ))

class Login( Strategy ):
	LOGIN_COOKIE = 'auth'
	LOGIN_OK = True
	ROLE_COOKIE = 'role'

	class ROLES( object ):
		USER = 0x1

	def __init__( 	self,
					creds = None,
					authed = None,
					deauthed = None,
					denied = None,
					failed = None,
					isauthed = None ):

		if( creds is None ):
			creds = Login._creds
		self.strategy( 'creds', creds )

		if( authed is None ):
			authed = Login._authed
		self.strategy( 'authed', authed )

		if( deauthed is None ):
			deauthed = Login._deauthed
		self.strategy( 'deauthed', deauthed )

		if( denied is None ):
			denied = Login._denied
		self.strategy( 'denied', denied )

		if( failed is None ):
			failed = Login._failed
		self.strategy( 'failed', failed )

		if( isauthed is None ):
			isauthed = Login._isauthed
		self.strategy( 'isauthed', isauthed )

	def required( self, role = ROLES.USER ):
		def decorator( func ):
			@wraps( func )
			def wrapper( *args, **kwargs ):
				if not self.isauthed( role ):
					self.denied( role )
				return func( *args, **kwargs )
			return wrapper
		return decorator

	def login( self, role =  ROLES.USER ):
		def decorator( func ):
			@wraps( func )
			def wrapper( *args, **kwargs ):
				if( self.creds( role ) ):
					self.authed( role )
				else:
					self.failed( role )
				return func( *args, **kwargs )
			return wrapper
		return decorator

	def logout( self, role = ROLES.USER ):
		def decorator( func ):
			@wraps( func )
			def wrapper( *args, **kwargs ):
				self.deauthed( role )
				return func( *args, **kwargs )
			return wrapper
		return decorator

	def _creds( self, role ): # simple auth
		if( request.args.get( 'user', '' ) == 'user' 
				and request.args.get( 'pass', '' ) == 'pass' ):
			return True
		return False

	def _authed( self, role ):
		session[ Login.LOGIN_COOKIE ] = Login.LOGIN_OK
		session[ Login.ROLE_COOKIE ] = role

	def _deauthed( self, role ):
		if Login.LOGIN_COOKIE in session:
			del session[ Login.LOGIN_COOKIE ]
		if Login.ROLE_COOKIE in session:
			del session[ Login.ROLE_COOKIE ]

	def _denied( self, role ): # when accessing restricted resource
		abort( 403 )

	def _failed( self, role ): # when authentication failed
		abort( 403 )

	def _isauthed( self, role ):
		if( Login.LOGIN_COOKIE in session 
				and session[ Login.LOGIN_COOKIE ] == Login.LOGIN_OK
				and Login.ROLE_COOKIE in session
				and session[ Login.ROLE_COOKIE ] == role ):
			return True
		return False
