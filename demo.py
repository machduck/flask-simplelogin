#!/usr/bin/env python

from flask import Flask
from flask import render_template_string as rts
from flask import Response as res

app = Flask( __name__ )
app.secret_key = "1234567890abcdef"

from simplelogin import Login
login = Login()

from xsrf import XSRFHandler
xsrfh = XSRFHandler( app )

@app.route( '/token' )
@xsrfh.send()
def token():
	return res(rts( '{{ xsrf_token }}' ))

@app.route( '/login' )
@xsrfh.verify()
@login.login()
def _login():
	return res( 'success' )

@app.route( '/logout' )
@xsrfh.verify()
@login.logout()
def _logout():
	return res( 'success' )

@app.route( '/secret' )
@login.required()
def secret():
	return res( 'secret' )

if __name__ == "__main__":
	app.run( debug = True )
